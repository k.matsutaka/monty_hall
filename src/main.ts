import { Door, Guest, MontyHall, Staff } from "./montyHallModule";

// 扉の個数
const doorCount = 3;
// 繰り返し実施する回数
const repearCount = 10000;
// 選びなおさない場合
repeatPlayMontyHall(repearCount, doorCount, false);
// 選びなおさした場合
repeatPlayMontyHall(repearCount, doorCount, true);

/**
 * モンティホール問題を繰り返し実施
 * @param repeatCount プレイ回数
 * @param doorCount 作成する扉の個数
 * @param reselectFlag 再選択するかどうか
 */
function repeatPlayMontyHall(repeatCount: number, doorCount: number, reselectFlag: boolean): void {
    let hitCount = 0;
    let notHitCount = 0;
    for (let i = 0; i < repeatCount; i++) {
        let result = playMontyHall(doorCount, reselectFlag);
        result ? hitCount++ : notHitCount++;
    }
    showResult(hitCount, notHitCount);
}

/**
 * 結果を表示する
 * @param hitCount あたり回数
 * @param notHitCount はずれ回数
 */
function showResult(hitCount: number, notHitCount: number): void {
    const totalCount = hitCount + notHitCount;
    const hitPersent = Math.floor(hitCount / totalCount * 100);
    console.log("あたり:" + hitCount + "回/はずれ:" + notHitCount + "回/正解率" + hitPersent + "%");
}

/**
 * モンティホール問題をプレイする。
 * @param doorCount 作成する扉の個数
 * @param reselectFlag 再選択フラグ
 * @returns プレイ結果
 */
function playMontyHall(doorCount: number, reselectFlag: boolean): boolean {
    // スタッフが扉を作成し正解を設定
    const staff = new Staff();
    const doorList = staff.createDoor(doorCount);
    staff.setHitToDoor(doorList);

    // ゲストが扉を選択
    const guest = new Guest();
    guest.selectDoor(doorList);

    // モンティホールが残りの扉の中からはずれの扉を開く
    const montyHall = new MontyHall();
    montyHall.openNotHitDoor(doorList, guest.getSelectedDoorIndex());

    // ゲストが再選択する
    if (reselectFlag) {
        guest.selectDoor(doorList);
    }

    // ゲストが選択した扉を開く
    const result = doorList[guest.getSelectedDoorIndex()].openDoor();
    return result;
}
