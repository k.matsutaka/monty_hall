/**
 * 扉クラス
 */
export class Door {
    /** 扉名 */
    private doorName: string;
    /** 正解・不正解フラグ */
    private hitFlag: boolean;
    /** 扉が開いているかどうか */
    private openStatus: boolean;

    /**
     * コンストラクタ
     * @param doorName 扉名
     */
    constructor(doorName: string) {
        this.doorName = doorName;
        this.hitFlag = false;
        this.openStatus = false;
    }

    /**
     * 扉名を取得する
     * @returns 扉名
     */
    getDoorName(): string {
        return this.doorName;
    }
    /**
     * 扉が開いているかどうか
     */
    getOpenStatus(): boolean {
        return this.openStatus;
    }
    /**
     * あたりを設定する
     * @param hitFlag 正解フラグ
     */
    setHitFlag(hitFlag: boolean): void {
        this.hitFlag = hitFlag;
    }

    /**
     * 当たりかはずれかを取得する
     */
    getHitFlag(): boolean {
        return this.hitFlag;
    }

    /**
     * 扉を開いて結果を表示
     * @returns あたりかはずれか
     */
    openDoor(): boolean {
        this.openStatus = true;
        return this.hitFlag;
    }
}

/**
 * スタッフクラス
 */
export class Staff {

    /**
     * 扉を作成する
     * @param doorCount 作成個数
     * @returns 扉リスト
     */
    createDoor(doorCount: number): Door[] {
        let retDoor: Door[] = [];
        for (let i = 0; i < doorCount; i++) {
            retDoor.push(new Door("扉" + (i + 1)));
        }
        return retDoor;
    }

    /**
     * 扉リストの中に正解を設定
     * @param doorList 扉リスト
     */
    setHitToDoor(doorList: Door[]): void {
        // 0～3の乱数を取得
        const index = Math.floor(Math.random() * doorList.length);
        doorList[index].setHitFlag(true);
    }
}

/**
 * ゲストクラス
 */
export class Guest {
    /** ゲストが選択した扉のindex */
    private selectedDoorIndex: number;

    /**
     * コンストラクター
     */
    constructor() {
        this.selectedDoorIndex = -1;
    }

    /**
     * ドアを選択する。
     * @param doorList 扉リスト
     */
    selectDoor(doorList: Door[]): void {
        while (true) {
            let index = Math.floor(Math.random() * doorList.length);
            // 未選択扉かつ開いていない扉を選択
            if (index !== this.selectedDoorIndex && !doorList[index].getOpenStatus()) {
                this.selectedDoorIndex = index;
                break;
            }
        }
    }

    /**
     * 選択したドアのIndex番号を取得する
     * @returns 選択したドアのIndex番号
     */
    getSelectedDoorIndex(): number {
        return this.selectedDoorIndex;
    }
}

/**
 * モンティホールクラス
 */
export class MontyHall {
    /**
     * ゲストが選択していないかつはずれの扉を開ける
     * @param doorList 扉リスト
     * @param guestSelectDoorIndex ゲストが選択した扉のIndex
     */
    openNotHitDoor(doorList: Door[], guestSelectDoorIndex: number): void {
        while (true) {
            let index = Math.floor(Math.random() * doorList.length);
            if (index !== guestSelectDoorIndex && !doorList[index].getHitFlag()) {
                doorList[index].openDoor();
                break;
            }
        }
    }
}